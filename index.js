import * as YAML from 'js-yaml';
export default yamlCode => {
    const obj = YAML.load(yamlCode);
    const statements = obj.statements;
    const table = document.createElement("table");
    const frag = document.createDocumentFragment();
    const headerTr = document.createElement("tr");
    headerTr.innerHTML = "<th>Statement</th><th>Reason</th>";
    frag.appendChild(headerTr);
    for (let stmt in statements) {
        const tr = document.createElement("tr");
        const stmtTd = document.createElement("td");
        const rsnTd = document.createElement("td");
        stmtTd.textContent = stmt;
        rsnTd.textContent = statements[stmt];
        tr.appendChild(stmtTd);
        tr.appendChild(rsnTd);
        frag.appendChild(tr);
    };
    table.appendChild(frag);
    return table;
}