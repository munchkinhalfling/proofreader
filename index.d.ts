module "@munchkinhalfling/proofreader" {
    export default function proofreader(yamlCode: string): HTMLTableElement;
}